module.exports = function redisCachingMiddleware(redisClient, key) {
    // eslint-disable-next-line func-names
    return function(req, res, next) {
        const params = req.params.id || '';
        const query = Object.keys(req.query)
            .reduce((a, v) => {
                a.push(`${v}=${req.query[v]}`)
                return a;
            }, [])
            .join("&");
        redisClient.get(`${key}_${params}_${query}`, (err, data) => {
            if (err) {
                console.log(err);
            }
            if (data) {
                return res.json(JSON.parse(data));
            }
            return next();
        });
    };
}