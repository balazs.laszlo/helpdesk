const userController= require('../../../adapters/controllers/userController');
const userDbRepository= require('../../../application/repositories/userDbRepository');
const userDbRepositoryMongoDB= require('../../database/mongoDB/repositories/userRepositoryMongoDB');
const authServiceInterface= require('../../../application/services/authService');
const authServiceImpl= require('../../services/authService');
const authMiddleware= require('../middlewares/authMiddleware');

module.exports = function userRouter(express) {
  const router = express.Router();

  // load controller with dependencies
  const controller = userController(
    userDbRepository,
    userDbRepositoryMongoDB,
    authServiceInterface,
    authServiceImpl
  );

  // GET enpdpoints
  router.route('/:id').get(authMiddleware, controller.fetchUserById);
  router.route('/').get(authMiddleware, controller.fetchUsersByProperty);

  // ticket enpdpoints
  router.route('/').post(controller.addNewUser);

  return router;
}
