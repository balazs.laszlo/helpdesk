const ticketRouter = require('./ticket')
const userRouter = require('./user')
const authRouter = require('./auth')
const validateRouter = require('./validate')


module.exports = function routes(app, express, redisClient) {
    app.use('/api/v1/tickets', ticketRouter(express, redisClient));
    app.use('/api/v1/users', userRouter(express, redisClient));
    app.use('/api/v1/login', authRouter(express, redisClient));
    app.use('/api/v1/islogin', validateRouter(express, redisClient));
}