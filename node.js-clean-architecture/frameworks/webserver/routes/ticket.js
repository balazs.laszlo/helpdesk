const ticketController = require('../../../adapters/controllers/ticketController')
const ticketDbRepository = require('../../../application/repositories/ticketDbRepository')
const ticketDbRepositoryMongoDB = require('../../database/mongoDB/repositories/ticketRepositoryMongoDB')
const ticketRedisRepository = require('../../../application/repositories/ticketRedisRepository')
const ticketRedisRepositoryImpl = require('../../database/redis/ticketRepositoryRedis')
const redisCachingMiddleware = require('../middlewares/redisCachingMiddleware')
const authMiddleware = require('../middlewares/authMiddleware')

module.exports = function ticketRouter(express, redisClient) {
  const router = express.Router();

  // load controller with dependencies
  const controller = ticketController(
    ticketDbRepository,
    ticketDbRepositoryMongoDB,
    redisClient,
    ticketRedisRepository,
    ticketRedisRepositoryImpl
  );

  // GET endpoints
  router
    .route('/')
    .get(
      [authMiddleware, redisCachingMiddleware(redisClient, 'tickets')],
      controller.fetchAlltickets
    );
  router
    .route('/:id')
    .get(
      [authMiddleware, redisCachingMiddleware(redisClient, 'ticket')],
      controller.fetchticketById
    );

  // ticket endpoints
  router.route('/').post(authMiddleware, controller.addNewticket);

  // PUT endpoints
  router.route('/:id').put(authMiddleware, controller.updateticketById);

  // DELETE endpoints
  router.route('/:id').delete(authMiddleware, controller.deleteticketById);

  return router;
}
