const validateController = require('../../../adapters/controllers/validateController');
const userDbRepository = require('../../../application/repositories/userDbRepository');
const userDbRepositoryMongoDB = require('../../database/mongoDB/repositories/userRepositoryMongoDB');
const authServiceInterface = require('../../../application/services/authService');
const authServiceImpl = require('../../services/authService');

module.exports = function validateRouter(express) {
    const router = express.Router();

    // load controller with dependencies
    const controller = validateController(
        userDbRepository,
        userDbRepositoryMongoDB,
        authServiceInterface,
        authServiceImpl
    );

    // ticket enpdpoints
    router.route('/').post(controller.validateUser);

    return router;
}