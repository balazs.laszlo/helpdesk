const authController= require('../../../adapters/controllers/authController');
const userDbRepository= require('../../../application/repositories/userDbRepository');
const userDbRepositoryMongoDB= require('../../database/mongoDB/repositories/userRepositoryMongoDB');
const authServiceInterface= require('../../../application/services/authService');
const authServiceImpl= require('../../services/authService');

module.exports = function authRouter(express) {
  const router = express.Router();

  // load controller with dependencies
  const controller = authController(
    userDbRepository,
    userDbRepositoryMongoDB,
    authServiceInterface,
    authServiceImpl
  );

  // ticket enpdpoints
  router.route('/').post(controller.loginUser);

  return router;
}
