const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const config = require('../../config/config');

module.exports = function authService() {
    const encryptPassword = (password) => {
        const salt = bcrypt.genSaltSync(10);
        return bcrypt.hashSync(password, salt);
    };

    const compare = (password, hashedPassword) =>
        bcrypt.compareSync(password, hashedPassword);

    const verify = (token) => jwt.verify(token, config.jwtSecret);

    const generateToken = (payload) =>
        jwt.sign(payload, config.jwtSecret, {
            expiresIn: '672h'
        });

    return {
        encryptPassword,
        compare,
        verify,
        generateToken
    };
}