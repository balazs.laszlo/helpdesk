const mongoose = require('mongoose');

// eslint-disable-next-line prefer-destructuring
const Schema = mongoose.Schema;
const ticketsSchema = new Schema({
    title: {
        type: String,
        unique: false
    },
    description: String,
    createdAt: {
        type: 'Date',
        default: Date.now
    },
    isPublished: {
        type: Boolean,
        default: false
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    comments: {
        type: [String]
    },
    state: {
        type: String
    }
});

ticketsSchema.index({ userId: 1, title: 1 });
ticketsSchema.index({ userId: 1, description: 1 });
ticketsSchema.index({ userId: 1, createdAt: 1 });
ticketsSchema.index({ userId: 1, isPublished: 1 });
ticketsSchema.index({ userId: 1, comments: 1 });
ticketsSchema.index({ userId: 1, state: 1 });

const ticketModel = mongoose.model('ticket', ticketsSchema);

ticketModel.ensureIndexes((err) => {
    if (err) {
        return err;
    }
    return true;
});

module.exports = ticketModel;