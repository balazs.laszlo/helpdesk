const ticketModel = require('../models/ticket');

function omit(obj, ...props) {
    const result = {...obj };
    props.forEach((prop) => delete result[prop]);
    return result;
}

module.exports = function ticketRepositoryMongoDB() {
    const findAll = (params) => {
        let a = ticketModel.find(omit(params, 'page', 'perPage'))
            .skip(params.perPage * params.page - params.perPage)
            .limit(params.perPage);
        return a;
    }

    const countAll = (params) =>
        ticketModel.countDocuments(omit(params, 'page', 'perPage'));

    const findById = (id) => ticketModel.findById(id);

    const add = (ticketEntity) => {
        const newticket = new ticketModel({
            title: ticketEntity.getTitle(),
            description: ticketEntity.getDescription(),
            createdAt: new Date(),
            isPublished: ticketEntity.isPublished(),
            userId: ticketEntity.getUserId(),
            comments: ticketEntity.getComments(),
            state: ticketEntity.getState()
        });

        return newticket.save();
    };

    const updateById = (id, ticketEntity) => {
        const updatedticket = {
            title: ticketEntity.getTitle(),
            description: ticketEntity.getDescription(),
            isPublished: ticketEntity.isPublished(),
            comments: ticketEntity.getComments(),
            state: ticketEntity.getState()
        };

        return ticketModel.findOneAndUpdate({ _id: id }, { $set: updatedticket }, { new: true });
    };

    const deleteById = (id) => ticketModel.findByIdAndRemove(id);

    return {
        findAll,
        countAll,
        findById,
        add,
        updateById,
        deleteById
    };
}