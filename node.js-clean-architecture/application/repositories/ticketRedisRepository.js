module.exports = function redisticketRepository(repository) {
  const setCache = (options) => repository.setCache(options);
  return {
    setCache
  };
}
