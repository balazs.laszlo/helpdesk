const ticket = require('../../../src/entities/ticket');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

module.exports = function updateById({
    id,
    title,
    description,
    createdAt,
    isPublished,
    userId,
    ticketRepository,
    comments,
    state
}) {
    // validate
    // if (!title || !description) {
    //   throw new Error('title and description fields are mandatory');
    // }
    const updatedticket = ticket({
        title,
        description,
        createdAt,
        isPublished,
        userId,
        comments,
        state
    });

    return ticketRepository.findById(ObjectId(id)).then((foundticket) => {
        if (!foundticket) {
            throw new Error(`No ticket found with id: ${id}`);
        }
        return ticketRepository.updateById(id, updatedticket);
    });
}