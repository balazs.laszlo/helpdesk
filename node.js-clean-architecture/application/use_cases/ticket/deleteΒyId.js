module.exports = function deleteById(id, ticketRepository) {
  return ticketRepository.findById(id).then((ticket) => {
    if (!ticket) {
      throw new Error(`No ticket found with id: ${id}`);
    }
    return ticketRepository.deleteById(id);
  });
}
