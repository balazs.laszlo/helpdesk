const ticket = require('../../../src/entities/ticket');

module.exports = function addticket({
    title,
    description,
    createdAt,
    isPublished,
    userId,
    ticketRepository,
    comments,
    state
}) {
    if (!title || !description) {
        throw new Error('title and description fields cannot be empty');
    }

    const newticket = ticket({ title, description, createdAt, isPublished, userId, comments, state });

    return ticketRepository.add(newticket);
}