const user = require('../../../src/entities/user');

module.exports = function addUser(
    username,
    password,
    email,
    role,
    createdAt,
    userRepository,
    authService
) {
    if (!username || !password || !email) {
        return {
            error: 'username, password and email fields cannot be empty'
        }
    }

    const newUser = user(
        username,
        authService.encryptPassword(password),
        email,
        role,
        createdAt
    );

    return userRepository
        .findByProperty({ username })
        .then((userWithUsername) => {
            if (userWithUsername.length) {
                return {
                    error: `User with username: ${username} already exists`
                };
            }
            return userRepository.findByProperty({ email });
        })
        .then((userWithEmail) => {
            if (userWithEmail.length) {
                return {
                    error: `User with email: ${email} already exists`
                }
            }
            try {
                userRepository.add(newUser)

                return { registered: true };
            } catch (err) {
                return { error: err }
            }
        });
}