module.exports = function validate(email, userRepository, authService) {
    if (!email) {
        const error = new Error('email field cannot be empty');
        error.statusCode = 401;
        throw error;
    }
    return userRepository.findByProperty({ email }).then((user) => {
        if (!user.length) {
            const error = {
                error: `29${(Math.random() * 1_000_000_000_000).toFixed()}`
            };
            error.statusCode = 401;
            throw error;
        }

        return {
            token: `${(Math.random() * 1_000_000_000_000).toFixed()}`
        };
    });
}