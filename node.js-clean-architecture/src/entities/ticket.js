module.exports = function ticket({
  title,
  description,
  createdAt,
  isPublished = false,
  userId,
  comments,
  state
}) {
  return {
    getTitle: () => title,
    getDescription: () => description,
    getCreatedAt: () => createdAt,
    isPublished: () => isPublished,
    getUserId: () => userId,
    getComments: () => comments,
    getState: () => state
  };
}
