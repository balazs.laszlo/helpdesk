/* eslint-disable no-unused-expressions */
const chai= require('chai');
const sinon= require('sinon');
const faker= require('faker');

const ticket= require('../../../../src/entities/ticket');
const addticket= require('../../../../application/use_cases/ticket/add');
const findAll= require('../../../../application/use_cases/ticket/findAll');
const findById= require('../../../../application/use_cases/ticket/findById');
const ticketDbRepository= require('../../../../application/repositories/ticketDbRepository');

const { expect } = chai;

let dbRepository = null;

describe('Use cases', () => {
  beforeEach(() => {
    dbRepository = ticketDbRepository();
  });

  describe('Fetch a specific ticket', () => {
    it('should fetch a ticket by id', () => {
      const stubticket = {
        title: faker.name.findName(),
        description: faker.name.findName(),
        createdAt: faker.date.past(),
        isPublished: false,
        userId: faker.random.uuid()
      };
      const correspondingticket = ticket({
        title: stubticket.title,
        description: stubticket.description,
        createdAt: stubticket.createdAt,
        isPublished: stubticket.isPublished,
        userId: stubticket.userId,
        ticketRepository: dbRepository
      });
      const stubRepositoryFindById = sinon
        .stub(dbRepository, 'findById')
        .returns(correspondingticket);
      const fetchedticket = findById('5fb1b12a6ac3e23493ac82e4', dbRepository);
      expect(stubRepositoryFindById.calledOnce).to.be.true;
      sinon.assert.calledWith(
        stubRepositoryFindById,
        '5fb1b12a6ac3e23493ac82e4'
      );
      expect(fetchedticket).to.eql(correspondingticket);
    });
  });

  describe('Fetch all tickets', () => {
    it('should fetch all the tickets succesfully', () => {
      const stubRepositoryFetchAll = sinon
        .stub(dbRepository, 'findAll')
        .returns(['ticket1', 'ticket2']);
      const tickets = findAll('602c13e0cfe08b794e1b287b', dbRepository);
      expect(stubRepositoryFetchAll.calledOnce).to.be.true;
      expect(tickets).to.eql(['ticket1', 'ticket2']);
    });
  });

  describe('Add new ticket', () => {
    it('should add a new ticket succesfully', () => {
      const stubValue = {
        title: faker.name.findName(),
        description: faker.name.findName(),
        createdAt: faker.date.past(),
        isPublished: false,
        userId: faker.random.uuid()
      };
      const pesristedticket = ticket({
        title: stubValue.title,
        description: stubValue.description,
        createdAt: stubValue.createdAt,
        isPublished: stubValue.isPublished,
        userId: stubValue.userId
      });
      const stubRepositoryAdd = sinon
        .stub(dbRepository, 'add')
        .returns(pesristedticket);
      const newticket = addticket({
        title: stubValue.title,
        description: stubValue.description,
        createdAt: stubValue.createdAt,
        isPublished: stubValue.isPublished,
        userId: stubValue.userId,
        ticketRepository: dbRepository
      });
      expect(stubRepositoryAdd.calledOnce).to.be.true;
      expect(newticket.getTitle()).equals(stubValue.title);
      expect(newticket.getDescription()).equals(stubValue.description);
      expect(newticket.getCreatedAt()).equals(stubValue.createdAt);
      expect(newticket.isPublished()).equals(stubValue.isPublished);
      expect(newticket.getUserId()).equals(stubValue.userId);
    });
  });
});
