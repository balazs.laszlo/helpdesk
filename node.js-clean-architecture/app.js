const express = require('express');
const mongoose = require('mongoose');
const redis = require('redis');
const config = require('./config/config.js');
const expressConfig = require('./frameworks/webserver/express');
const routes = require('./frameworks/webserver/routes');
const serverConfig = require('./frameworks/webserver/server');
const mongoDbConnection = require('./frameworks/database/mongoDB/connection');
const redisConnection = require('./frameworks/database/redis/connection');
// middlewares
const errorHandlingMiddleware = require('./frameworks/webserver/middlewares/errorHandlingMiddleware');

const app = express();
app.use((
    req, _res, next
) => {
    console.log(
        'Time:', Date.now(), 'method: ', req.method, 'path: ', req.originalUrl
    );
    next()
});
const server = require('http').createServer(app);


// express.js configuration (middlewares etc.)
expressConfig(app);

// server configuration and start
serverConfig(app, mongoose, server, config).startServer();

// DB configuration and connection create
mongoDbConnection(mongoose, config, {
    autoIndex: false,
    useCreateIndex: true,
    useNewUrlParser: true,
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 10000,
    keepAlive: 120,
    connectTimeoutMS: 1000
}).connectToMongo();

const redisClient = redisConnection(redis, config).createRedisClient();

// routes for each endpoint
routes(app, express, redisClient);

// error handling middleware
app.use(errorHandlingMiddleware);

// Expose app
module.exports = app;