const findAll = require('../../application/use_cases/ticket/findAll');
const countAll = require('../../application/use_cases/ticket/countAll');
const addticket = require('../../application/use_cases/ticket/add');
const findById = require('../../application/use_cases/ticket/findById');
const updateById = require('../../application/use_cases/ticket/updateById');
const deleteticket = require('../../application/use_cases/ticket/deleteΒyId');

module.exports = function ticketController(
    ticketDbRepository,
    ticketDbRepositoryImpl,
    cachingClient,
    ticketCachingRepository,
    ticketCachingRepositoryImpl
) {
    const dbRepository = ticketDbRepository(ticketDbRepositoryImpl());
    const cachingRepository = ticketCachingRepository(
        ticketCachingRepositoryImpl()(cachingClient)
    );

    // Fetch all the tickets of the logged in user
    const fetchAlltickets = (req, res, next) => {
        const params = {};
        const response = {};

        // Dynamically created query params based on endpoint params
        for (const key in req.query) {
            if (Object.prototype.hasOwnProperty.call(req.query, key)) {
                params[key] = req.query[key];
            }
        }
        // predefined query params (apart= require(dynamically) for pagination
        // and current logged in user
        params.page = params.page ? parseInt(params.page, 10) : 1;
        params.perPage = params.perPage ? parseInt(params.perPage, 10) : 10;
        if (req.user.role !== "support") {
            params.userId = req.user.id;
        }

        if (params.keywords) {
            let keywords = JSON.parse(params.keywords);
            delete params.keywords;
            if (keywords.length > 0) {
                params.$or = [{
                        title: {
                            $in: keywords.map(s => {
                                return new RegExp(`.*${s}.*`)
                            })
                        }
                    },
                    {
                        description: {
                            $in: keywords.map(s => {
                                return new RegExp(`.*${s}.*`)
                            })
                        }
                    },
                ]
            }
        }

        findAll(params, dbRepository)
            .then((tickets) => {
                response.tickets = tickets;
                const cachingOptions = {
                    key: 'tickets_',
                    expireTimeSec: 30,
                    data: JSON.stringify(tickets)
                };
                // cache the result to redis
                cachingRepository.setCache(cachingOptions);
                return countAll(params, dbRepository);
            })
            .then((totalItems) => {
                response.totalItems = totalItems;
                response.totalPages = Math.ceil(totalItems / params.perPage);
                response.itemsPerPage = params.perPage;
                return res.json(response);
            })
            .catch((error) => next(error));
    };

    const fetchticketById = (req, res, next) => {
        findById(req.params.id, dbRepository)
            .then((ticket) => {
                if (!ticket) {
                    throw new Error(`No ticket found with id: ${req.params.id}`);
                }
                res.json(ticket);
            })
            .catch((error) => next(error));
    };

    const addNewticket = (req, res, next) => {
        const { title, description } = req.body;

        addticket({
                title,
                description,
                userId: req.user.id,
                ticketRepository: dbRepository,
                comments: [],
                state: "sent"
            })
            .then((ticket) => {
                const cachingOptions = {
                    key: 'tickets_',
                    expireTimeSec: 30,
                    data: JSON.stringify(ticket)
                };
                // cache the result to redis
                cachingRepository.setCache(cachingOptions);
                return res.json('ticket added');
            })
            .catch((error) => next(error));
    };

    const deleteticketById = (req, res, next) => {
        deleteticket(req.params.id, dbRepository)
            .then(() => res.json('ticket sucessfully deleted!'))
            .catch((error) => next(error));
    };

    const updateticketById = (req, res, next) => {
        const { title, description, isPublished, comments, state } = req.body;

        updateById({
                id: req.params.id,
                title,
                description,
                userId: req.user.id,
                isPublished,
                ticketRepository: dbRepository,
                comments,
                state
            })
            .then((message) => res.json(message))
            .catch((error) => next(error));
    };

    return {
        fetchAlltickets,
        addNewticket,
        fetchticketById,
        updateticketById,
        deleteticketById
    };
}