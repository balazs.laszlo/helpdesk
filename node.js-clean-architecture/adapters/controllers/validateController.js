const validate = require('../../application/use_cases/validate/validate');

module.exports = function validateController(
    userDbRepository,
    userDbRepositoryImpl,
    authServiceInterface,
    authServiceImpl
) {
    const dbRepository = userDbRepository(userDbRepositoryImpl());
    const authService = authServiceInterface(authServiceImpl());

    const validateUser = (req, res, next) => {
        const { email } = req.body;
        validate(email, dbRepository, authService)
            .then((resp) => res.json(resp))
            .catch((err) => next(err));
    };
    return {
        validateUser
    };
}