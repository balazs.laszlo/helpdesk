import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { chunkSplitPlugin } from 'vite-plugin-chunk-split';
import * as path from 'path';

const backendUrl = process.env.BACKEND_URL ?? "127.0.0.1";
const backendPort = process.env.BACKEND_PORT ?? "1234";
const ownPort = process.env.PORT ?? "1235";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react({}),
    chunkSplitPlugin({
      strategy: 'single-vendor',
      customChunk: (args) => {
        let { file, id, moduleId, root } = args;
        if (file.indexOf('src/features/') == 0) {
          let filename = file.split("/")[2];
          return "pages/" + filename;
        }
        if (file.indexOf('node_modules/') == 0) {
          let packagename = file.split("/")[1];
          return "include/" + packagename;
        }
        return null;
      },
    })
  ],
  build: {
    rollupOptions: {
      treeshake: false,
    },
    chunkSizeWarningLimit: 200,
  },
  resolve: {
    alias: {
      "devextreme/animation": 'devextreme/esm/animation',
      "devextreme/core": 'devextreme/esm/core',
      "devextreme/data": 'devextreme/esm/data',
      "devextreme/events": 'devextreme/esm/events',
      "devextreme/exporter": 'devextreme/esm/exporter',
      "devextreme/file_management": 'devextreme/esm/file_management',
      "devextreme/integration": 'devextreme/esm/integration',
      "devextreme/localization": 'devextreme/esm/localization',
      "devextreme/mobile": 'devextreme/esm/mobile',
      "devextreme/renovation": 'devextreme/esm/renovation',
      "devextreme/ui": 'devextreme/esm/ui',
      "devextreme/viz": 'devextreme/esm/viz',
      "~": path.resolve(__dirname, 'src')
    }
  },
  preview: {
    port: Number(ownPort),
    proxy: {
      '/api': {
        target: `http://${backendUrl}:${backendPort}/`,
        changeOrigin: true,
      },
    },
  },
  server: {
    port: Number(ownPort),
    proxy: {
      '/api': {
        target: `http://${backendUrl}:${backendPort}/`,
        changeOrigin: true,
      },
    },
  }
})
