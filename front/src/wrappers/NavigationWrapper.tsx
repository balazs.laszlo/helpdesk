import { Outlet } from "react-router-dom";
import Navigation from '../features/navigation/Navigation';

export function NavigationWrapper() {
  return (
    <>
      <Navigation />
      <div className='body_container'>
        <Outlet />
      </div>
    </>
  );
}
