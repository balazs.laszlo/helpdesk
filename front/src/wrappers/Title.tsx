import { useLocalizations } from '../app/hooks';
import { Helmet } from "react-helmet";
import { LanguageKeys } from '../localization/LanguageTypes';

type TitleProps = {
  titleKey: LanguageKeys;
  title?: string;
  children: any;
};
export function Title(props: TitleProps) {
  const { titleKey, children, title } = props;
  const strings = useLocalizations();
  return <>
    <Helmet>
      <title>{titleKey === "none" ? title : strings.getString(titleKey)}</title>
    </Helmet>
    {children}
  </>;
}
