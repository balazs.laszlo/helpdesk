import { LocalizedStringsMethods } from "react-localization";

/**
 * Language type for guarding all laguage options.
 * 
 * A key should look like: <name of page/function/class>_<keyword/s>
 * If using multiple keywords separate them with _ (underscore)
 * 
 * example: <key>?: string;
 * 
 * The types are sting!
 * Don't use nesting!
 * 
 * Order it based on page/function/class than alphabeticaly!
 */
export class LanguageType {

  description?: string;

  new_ticket?: string;
  missing_ticket_title?: string;
  missing_ticket_description?: string;
  ticket_saved?: string;
  ticket_obj_title?: string;
  ticket_obj_description?: string;
  user?: string;

  account_menu_profile?: string;
  account_menu_voucher?: string;
  account_menu_voucher_invoice?: string;

  add_to_cart?: string;
  add_to_cart_success?: string;
  add_to_cart_failed?: string;
  add_to_cart_sortiments?: string;

  admin_menu_title?: string;
  admin_menu_stockpile?: string;
  admin_menu_stock?: string;
  admin_menu_stock_details?: string;
  admin_menu_cache?: string;
  admin_menu_banner?: string;
  admin_menu_backtosite?: string;
  admin_menu_toadmin?: string;
  admin_menu_vouchertype?: string;
  admin_menu_stockgroup?: string;
  admin_menu_parameters?: string;
  admin_menu_logo?: string;
  admin_menu_stock_virtualsortiment?: string;
  admin_menu_stock_visualisation?: string;
  admin_menu_frxreport?: string;

  admin_param_use_wishlist_name?: string;
  admin_param_use_wishlist_comment?: string;
  admin_param_show_original_price_name?: string;
  admin_param_show_original_price_comment?: string;
  admin_param_show_level_of_stock_name?: string;
  admin_param_show_level_of_stock_comment?: string;
  admin_param_use_stockpile_name?: string;
  admin_param_use_stockpile_comment?: string;
  admin_param_webshop_warehouse_name?: string;
  admin_param_webshop_warehouse_comment?: string;
  admin_param_show_price_without_login_name?: string;
  admin_param_show_price_without_login_comment?: string;
  admin_param_default_pricetype_name?: string;
  admin_param_default_pricetype_comment?: string;
  admin_param_default_vouchertype_name?: string;
  admin_param_default_vouchertype_comment?: string;
  admin_param_default_employee_name?: string;
  admin_param_default_employee_comment?: string;
  admin_param_use_invoices_name?: string;
  admin_param_use_invoices_comment?: string;
  admin_param_use_virtual_sortiment_name?: string;
  admin_param_use_virtual_sortiment_comment?: string;
  admin_param_use_company_visualisation_name?: string;
  admin_param_use_company_visualisation_comment?: string;
  admin_param_default_record_state_head_name?: string;
  admin_param_default_record_state_head_comment?: string;
  admin_param_use_pricetype_from_company_companygroup_switch_name?: string;
  admin_param_use_pricetype_from_company_companygroup_switch_comment?: string;
  admin_param_check_debt_name?: string;
  admin_param_check_debt_comment?: string;

  banner_delete?: string;
  banner_left?: string;
  banner_right?: string;

  cache_title?: string;
  cache_in_progress?: string;
  cache_in_progress_0?: string;
  cache_title_all?: string;
  cache_title_stock?: string;
  cache_title_user?: string;
  cache_all?: string;
  cache_stock_all?: string;
  cache_user_all?: string;
  cache_user?: string;
  cache_vouchertype?: string;
  cache_voucher?: string;
  cache_sync_completed?: string;
  cache_sync_failed?: string;
  cache_sync_started?: string;
  cache_title_other?: string;
  cache_other_all?: string;
  cache_stock?: string;
  cache_stockpile?: string;
  cache_stockprice?: string;
  cache_stockgroup?: string;
  cache_deviza?: string;
  cache_measurement?: string;
  cache_title_images?: string;
  cache_stock_image?: string;
  cache_stockgroup_image?: string;
  cache_paymentmethod?: string;
  cache_country?: string;
  cache_county?: string;
  cache_all_image?: string;
  cache_pricetype?: string;
  cache_employee?: string;
  cache_warehouse?: string;
  cache_recordstatehead?: string;

  cache_obj_typename?: string;
  cache_obj_day?: string;
  cache_obj_hour?: string;
  cache_obj_minute?: string;
  cache_obj_second?: string;
  cache_obj_isactive?: string;
  cache_obj_nextrun?: string;

  cache_type_deviza?: string;
  cache_type_measurement?: string;
  cache_type_stock?: string;
  cache_type_stockgroup?: string;
  cache_type_stockprice?: string;
  cache_type_user?: string;
  cache_type_employee?: string;
  cache_type_image?: string;
  cache_type_paymentmethods?: string;
  cache_type_pricetype?: string;
  cache_type_stockpile?: string;
  cache_type_transportmode?: string;
  cache_transportmode?: string;
  cache_type_voucher?: string;
  cache_type_vouchertype?: string;

  calculating?: string;

  cart_title?: string;
  cart_continue?: string;
  cart_summary_title?: string;
  cart_summary_total?: string;
  cart_summary_total_net?: string;
  cart_summary_total_vat?: string;
  cart_summary_total_gross?: string;
  cart_loading?: string;
  cart_delete?: string;
  cart_products?: string;
  cart_addresses?: string;
  cart_add?: string;
  cart_transports?: string;
  cart_payments?: string;
  cart_comment?: string;
  cart_total?: string;
  cart_empty?: string;
  cart_details?: string;
  cart_net_value?: string;
  cart_gross_value?: string;

  cart_address_city?: string;
  cart_address_streetaddress?: string;
  cart_address_county?: string;
  cart_address_country?: string;
  cart_address_postalcode?: string;
  cart_addresses_loading?: string;

  cart_delete_item_title?: string;
  cart_delete_item_content?: string;
  cart_delete_item_success?: string;
  cart_delete_item_failed?: string;

  fastreport_type_voucher?: string;
  fastreport_usetype_voucher_details?: string;

  frx_obj_name?: string;
  frx_obj_type?: string;
  frx_obj_lang?: string;
  frx_obj_file?: string;

  image_reload?: string;

  login_title?: string;
  login?: string;
  login_back?: string;
  login_continue?: string;
  login_email?: string;
  login_username?: string;
  login_error_missing_username_or_email?: string;
  login_password?: string;
  login_error_password?: string;
  login_error_email?: string;
  login_error_username?: string;

  menu_home?: string;
  menu_products?: string;
  menu_myaccount?: string;
  menu_favorites?: string;
  menu_cart?: string;
  menu_logout?: string;
  menu_login?: string;
  menu_login_title?: string;

  msg_login?: string;
  msg_debt?: string;
  msg_debt_title?: string;

  pagination_next?: string;
  pagination_prev?: string;

  parameters_title?: string;
  parameters_sys_params_title?: string;
  parameters_sys_params_back?: string;
  parameters_sys_params_password?: string;

  parameters_obj_name?: string;
  parameters_obj_flag?: string;
  parameters_obj_comment?: string;
  parameters_obj_value?: string;

  profile_title?: string;

  register_can_send_notifications?: string;
  register_password?: string;
  register_password2?: string;
  register_pasword_missmatch?: string;
  register_title?: string;
  register_small_title?: string;
  register_terms_conditions?: string;
  register_username?: string;

  search_keywords?: string;
  search_results?: string;

  sorting_sort_by_label?: string;
  sorting_sort_by_alphabet?: string;
  sorting_sort_by_price_asc?: string;
  sorting_sort_by_price_decr?: string;
  sorting_page_size?: string;

  status?: string;

  stock_obj_name?: string;
  stock_obj_stockno?: string;
  stock_obj_image?: string;
  stock_obj_altstockno?: string;
  stock_obj_altstockno1?: string;
  stock_obj_altstockno2?: string;
  stock_obj_barcode?: string;
  stock_obj_comment?: string;
  stock_obj_comment2?: string;
  stock_obj_grossweight?: string;
  stock_obj_netweight?: string;
  stock_obj_maxquantity?: string;
  stock_obj_minorderquantity?: string;
  stock_obj_md1?: string;
  stock_obj_md2?: string;
  stock_obj_md3?: string;
  stock_obj_metakeywords?: string;
  stock_obj_metadescription?: string;
  stock_obj_metatitle?: string;
  stock_obj_packquantity?: string;
  stock_obj_palletquantity?: string;
  stock_obj_sortname?: string;
  stock_obj_stockmaterial?: string;
  stock_obj_supply?: string;
  stock_obj_title?: string;
  stock_obj_stocksize?: string;
  stock_obj_stockcolor?: string;
  stock_obj_measurement?: string;

  stock_details_button?: string;
  stock_visualisation_manage?: string;

  stockgroup_obj_image?: string;
  stockgroup_obj_name?: string;
  stockgroup_obj_code?: string;
  stockgroup_obj_comment?: string;

  stockpile_obj_quantity?: string;
  stockpile_obj_sarzs?: string;
  stockpile_obj_sortiments?: string;

  virtualsortiment_add_item?: string;
  virtualsortiment_add_title?: string;
  virtualsortiment_add?: string;

  virtualsortiment_save?: string;
  virtualsortiment_delete?: string;
  virtualsortiment_save_failed?: string;
  virtualsortiment_missing_name?: string;

  virtualsortiment_obj_name?: string;
  virtualsortiment_obj_stockno?: string;

  invoice_title?: string;
  order_title?: string;
  voucher_search_vouchernumber?: string;

  order_details_title?: string;
  invoice_details_title?: string;
  voucher_details_products?: string;
  voucher_details_order?: string;
  voucher_details_loading?: string;
  voucher_details_payment_method?: string;
  voucher_details_payed?: string;
  voucher_details_not_payed?: string;
  voucher_details_order_again?: string;
  voucher_details_total_payed?: string;
  voucher_details_invoice?: string;
  voucher_details_invoice_details?: string;
  voucher_details_invoice_name?: string;
  voucher_details_invoice_address?: string;
  voucher_details_transport_mode?: string;
  voucher_details_transport_name?: string;
  voucher_details_transport_address?: string;
  voucher_details_voucherdate?: string;
  voucher_details_grossvalue?: string;
  voucher_details_net_payed?: string;
  voucher_details_vat_payed?: string;
  voucher_details_gross_payed?: string;
  voucher_details_products_sent?: string;
  voucher_details_transport_mode_title?: string;
  voucher_details_transport_date?: string;
  voucher_details_transport_date_estimating?: string;

  voucher_obj_vouchernumber?: string;
  voucher_obj_voucherdate?: string;
  voucher_obj_grossvalue?: string;
  voucher_obj_deviza?: string;
  voucher_obj_vouchertype?: string;
  voucher_obj_comment?: string;

  vouchertype_title?: string;

  vouchertype_obj_isactive?: string;
  vouchertype_obj_usetype?: string;
  vouchertype_obj_name?: string;
  vouchertype_obj_prefix?: string;

  loading?: string;
  deletee?: string;
  save?: string;
  visible?: string;
  unvisible?: string;
  download?: string;
  none?: string;
  error404?: string;
  search_title?: string;
}
export type LanguageKeys = keyof LanguageType;

export class CustomLocalizedStrings implements LocalizedStringsMethods {
  localizedStrings: LocalizedStringsMethods;
  constructor(localizedStrings: LocalizedStringsMethods) {
    this.localizedStrings = localizedStrings;
  }

  setLanguage(lang: string) {
    return this.localizedStrings.setLanguage(lang);
  }
  getLanguage() {
    return this.localizedStrings.getLanguage();
  }
  getInterfaceLanguage() {
    return this.localizedStrings.getInterfaceLanguage();
  }
  formatString<T extends string | number | JSX.Element>(str: string, ...values: (T | { [key: string]: T; })[]): string | (string | T)[] {
    return this.localizedStrings.formatString(str, ...values);
  }
  setContent(props: any): void {
    return this.localizedStrings.setContent(props);
  }
  getAvailableLanguages() {
    return this.localizedStrings.getAvailableLanguages();
  }
  getString(key: LanguageKeys) {
    let val = this.localizedStrings.getString(key, this.localizedStrings.getLanguage(), false);
    if (val?.length > 0) return val;
    return `<"${key}">`;
  }
  getCustomString(baseKey: string, otherKey: string) {
    let key = `${baseKey.toLowerCase()}_${otherKey.toLocaleLowerCase()}`;
    let val = this.localizedStrings.getString(key, this.localizedStrings.getLanguage(), false);
    if (val?.length > 0) return val;
    return `<"${key}">`;
  }
}