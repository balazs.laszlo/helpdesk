import { useEffect } from 'react';
import setupIndexedDB, { useIndexedDBStore } from 'use-indexeddb';

const idbConfig = {
  databaseName: "partnerimages",
  version: 1,
  stores: [
    {
      name: "images",
      id: { keyPath: "id", autoIncrement: true },
      indices: [
        { name: "imgpath", keyPath: "imgpath", options: { unique: true } },
        { name: "images", keyPath: "images" },
        { name: "timestamp", keyPath: "timestamp" },
      ],
    },
  ],
};
export function useIndexedDb(storeName: string) {
  useEffect(() => {
    setupIndexedDB(idbConfig)
      .then(() => { })
      .catch(e => console.error("idberror error / unsupported", e));
  }, []);

  return useIndexedDBStore(storeName);
}
