import { AxiosResponse } from "axios";
import { useDispatch } from "react-redux";
import { logout } from "../app/customslice/authSlice";

export function useHandleAxiosExcepons() {
  const dispatch = useDispatch();
  return (r: AxiosResponse<any, any>) => {
    switch (r.status) {
      case 200: return;
      case 401: {
        dispatch(logout());
        break;
      }
    }
  };
}
