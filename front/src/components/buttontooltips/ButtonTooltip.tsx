import { Tooltip } from 'devextreme-react';
import { useRef } from 'react';


export type ButtonTooltipProps = {
  target: string;
  show: boolean;
  renderChildren?: (hide: Function) => JSX.Element;
};

export function ButtonTooltip(props: ButtonTooltipProps) {
  const { target, show, renderChildren } = props;

  const tooltipRef = useRef<Tooltip>(null);
  let ts: NodeJS.Timeout;

  let hideTooltip = () => tooltipRef.current?.instance.hide();

  const tooltipDivRef = useRef<HTMLDivElement>(null);

  if (show) {
    return (
      <Tooltip
        ref={tooltipRef}
        target={target}
        showEvent={'mouseenter'}
        hideEvent={'mouseout1'}
        position='bottom'
        onShowing={() => {
          ts = setTimeout(() => {
            hideTooltip();
          }, 2000);
        }}
        showCloseButton={true}
        hideOnOutsideClick={false}
        
        contentRender={() => {
          return (
            <div
              ref={tooltipDivRef}
              style={{
                display: 'grid',
                gridTemplateColumns: '1fr'
              }}
              onMouseLeave={() => {
                hideTooltip();
              }}
              onMouseEnter={
                (e) => {
                  clearTimeout(ts);
                  tooltipDivRef.current?.addEventListener('wheel',
                    (e) => {
                      e.stopPropagation();
                    }
                  )
                }}
            >
              {renderChildren && renderChildren(hideTooltip)}
            </div>
          );
        }}
      />
    );
  }
  return (<></>);
}
