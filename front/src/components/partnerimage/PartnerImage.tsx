import placeholder from '../../assets/placeholder.jpg';
import { useEffect, useState } from 'react';
import { Gallery } from 'devextreme-react';

interface PartnerImageParams {
  name: string;
  datauri?: string;

  width?: string;
  height?: string;

  isGalery?: boolean;
}
export function PartnerImage(this: any, params: PartnerImageParams) {
  const { name, datauri, width, height, isGalery } = params;

  const [source, _setSource] = useState(placeholder);
  const [sources, _setSources] = useState([placeholder]);

  function setSource(images: string[]) {
    _setSource(images[0])
    _setSources(images)
  }

  useEffect(() => {
    if (datauri) {
      setSource([datauri]);
    }
  }, [datauri])

  return (
    <div
      style={{
        width: width ?? "100%",
        height: height ?? "auto",
      }}
    >
      {isGalery
        ? <span 
        style={{
          pointerEvents: 'none',
          maxHeight: 'inherit',
          maxWidth: 'inherit',
        }}
        >
          <Gallery
            dataSource={sources}
            height="100%"
            width="100%"
            slideshowDelay={3000}
            loop={true}
            showIndicator={false}
          />
        </span>
        : <img
          style={{
            maxWidth: '100%',
            maxHeight: '100%',
          }}
          src={source}
          alt={name}
        />
      }
    </div>
  );
}