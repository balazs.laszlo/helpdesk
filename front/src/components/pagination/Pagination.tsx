import ReactPaginate from "react-paginate";
import css from './Pagination.module.scss'
import { FontAwesome } from "~/assets/FontAwesome";

export type PaginationProps = {
  handlePageClick: (selectedItem: { selected: number; }) => void;
  displayedPageRange: number;
  pageCount: number;
  initialPage?: number;
  forcePage?: number | undefined;
};
export default function Pagination(props: PaginationProps) {
  const { handlePageClick, displayedPageRange, pageCount, initialPage, forcePage } = props;

  return (
    <ReactPaginate
        activeClassName={css.active_page_number}
        className={css.paginator}
        pageClassName={css.page_number}
        previousClassName={css.prev_button}
        nextClassName={css.next_button}
        disabledClassName={css.disabled_page_button}
        breakLabel=" ... "
        breakClassName={css.beak_label}
        nextLabel={<i className={FontAwesome.arrow_right}></i>}
        onPageChange={handlePageClick}
        pageRangeDisplayed={displayedPageRange}
        initialPage={initialPage || 0}
        pageCount={pageCount}
        previousLabel={<i className={FontAwesome.arrow_left}></i>}
        forcePage={forcePage}
      />
  );
}
