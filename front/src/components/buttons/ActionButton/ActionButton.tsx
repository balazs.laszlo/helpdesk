import { MouseEventHandler } from 'react';
import { useLocalizations } from '~/app/hooks';
import { FontAwesome } from '~/assets/FontAwesome';
import { LanguageKeys } from '~/localization/LanguageTypes';
import css from './ActionButton.module.scss'

export type ActionButtonProps = {
  onClick: MouseEventHandler<HTMLDivElement>,
  icon: FontAwesome,
  titleKey: LanguageKeys
};
export default function ActionButton(props: ActionButtonProps) {
  const { onClick, icon, titleKey } = props;
  const strings = useLocalizations();

  return (
  <div className={css.cart_button} onClick={onClick}>
    <div className={css.cart_icon_holder}>
      <i className={icon}></i>
    </div>
    <div className={css.cart_text_holder}>
      <span className={css.cart_button_text}>{strings.getString(titleKey)}</span>
    </div>
  </div>
  );
}
