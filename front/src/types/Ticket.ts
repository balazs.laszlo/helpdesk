
export type Ticket = {
  _id: number;
  title: string;
  description: string;
  userId: string;
  isPublished: string;
  createdAt: string;
  comments: any[];
  state: string;
}