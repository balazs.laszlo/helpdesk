export function stringVal(str: string): string | undefined {
  if (str) {
    return str.trim().length > 0 ? str.trim() : undefined;
  }
  return undefined;
}