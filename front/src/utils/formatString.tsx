export function formatString(str: string, ...args: string[]): string {
  let result = str;
  for (let i = 0; i < args.length; i++) {
    result = result.replace(new RegExp("\\{" + i + "\\}", "gi"), args[i]);
  }
  return result;
}
