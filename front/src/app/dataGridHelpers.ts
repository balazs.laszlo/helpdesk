import moment from "moment/moment";

const dateFormat = 'YYYY. MM. DD.';

export const convertBirthDays = (result: any[]) => {
    result.forEach(participant => {
        participant.birthday = moment.utc(participant.birthday).format(dateFormat);
    })
}

export const dateSorting = (date1: string, date2: string) => {
    return moment(date1, dateFormat).isBefore(moment(date2, dateFormat)) ? -1 : 1;
}