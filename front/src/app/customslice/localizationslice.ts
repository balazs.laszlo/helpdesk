// DUCKS pattern
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import LocalizedStrings from 'react-localization';
import { LanguageType, CustomLocalizedStrings } from '../../localization/LanguageTypes';
let dir = import.meta.glob('/public/localization/*.json', { eager: true });
if (Object.keys(dir).length == 0) {
  dir = import.meta.glob('/localization/*.json', { eager: true });
}

const keys = Object.keys(new LanguageType());
const langErrors: { [key: string]: string[] } = {
  type: []
}

const checkForErrors = localStorage.getItem("checkForLanguageErrors") == "true";

const localizationObject: any = {};
for (const jsonName in dir) {
  let json: any = dir[jsonName]
  let langname = jsonName.substring(jsonName.lastIndexOf('/') + 1).replace(".json", "");
  localizationObject[langname] = json;
  langErrors[langname] = [];
  if (checkForErrors) {
    let currentKeys = Object.keys(json);
    for (let key of currentKeys) {
      if (!keys.includes(key)) {
        if (langErrors["type"])
          langErrors["type"].push(key)
      }
    }
    let missing = keys.filter(s => currentKeys.indexOf(s) == -1)
    for (let key of missing) {
      langErrors[langname].push(key)
    }
  }
}

if (checkForErrors) {
  console.log("langErrors", langErrors)
  if (Object.values(langErrors).some(s => s.length > 0)) {
    throw "Language errors!"
  }
}

let strings = new CustomLocalizedStrings(new LocalizedStrings<LanguageType>(localizationObject, {
  logsEnabled: true,
  customLanguageInterface() {
    return localStorage.getItem("lang") ?? "ro";
  },
}));

interface LocalizationSate {
  allStrings: CustomLocalizedStrings;
  currentLanguage: string;
  languages: string[];
  errors: any;
}
const initialState: LocalizationSate = {
  allStrings: strings,
  currentLanguage: strings.getLanguage(),
  languages: strings.getAvailableLanguages(),
  errors: langErrors,
};

const localizationSlice = createSlice({
  name: 'localization',
  initialState,
  reducers: {
    setLanguage(state, action: PayloadAction<string>) {
      let lang = action.payload;
      if (!state.languages.includes(lang)) {
        lang = state.languages[0];
      }
      localStorage.setItem("lang", lang)
      state.currentLanguage = lang;
      state.allStrings.setLanguage(lang);
    },
  },
});

export const { setLanguage } = localizationSlice.actions;
export default localizationSlice.reducer;