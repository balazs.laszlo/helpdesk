// DUCKS pattern
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useAppSelector } from '../hooks';

export function getLoginOptionsFromThunk(thunkAPI: any) {
  const token: string = (thunkAPI.getState() as any)?.auth?.token ?? "";
  return {
    headers: [
      ['authorization', token]
    ]
  };
}

export type User = {
  email: string;
  username: string;
};

export interface LoginRequest {
  email?: string;
  username?: string;
  password: string;
}

export const login = createAsyncThunk('auth/login', async (params: LoginRequest, thunkAPI) => {
  let resp = await fetch("/api/v1/login", {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(params)
  })

  let jsonresp = await resp.json();
  console.log(jsonresp);

  thunkAPI.dispatch(authSlice.actions.setIsSupport(jsonresp.role))

  return jsonresp;
})
export interface IsLoginRequest {
  email?: string;
  username?: string;
}
export const isLogin = createAsyncThunk('auth/islogin', async (params: IsLoginRequest, thunkAPI) => {
  let apiresp = await fetch("/api/v1/islogin", {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(params)
  })
  return await apiresp.json();
})

export interface RegisterRequest {
  email: string;
  password: string;
  name: string;
  agreeTerms: string;
  canSendNotifications: string;
}

export const register = createAsyncThunk('auth/register', async (params: RegisterRequest, thunkAPI) => {
  let resp = await fetch("/api/v1/users", {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: params.email,
      username: params.name,
      password: params.password,
      role: params.canSendNotifications === "1" ? "support" : null
    })
  })
  return await resp.json();
})

interface AuthSate {
  token: string;
  isSupport: boolean;
  email?: string;
}
const initialState: AuthSate = {
  token: localStorage.getItem('token') ?? "",
  isSupport: JSON.parse(localStorage.getItem('isSupport') ?? "false") as boolean ?? false,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout(state) {
      state.token = "";
      localStorage.setItem("token", '')
      location.reload()
    },
    setEmail(state, action: PayloadAction<string | undefined>) {
      state.email = action.payload;
    },
    setIsSupport(state, action: PayloadAction<string | undefined>) {
      state.isSupport = action.payload === "support";
      localStorage.setItem("isSupport", JSON.stringify(state.isSupport))
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state) => {
        state.token = "";
        localStorage.setItem("token", '')
      })
      .addCase(login.fulfilled, (state, action) => {
        state.token = action.payload.token;
        localStorage.setItem("token", state.token)
      })
      .addCase(login.rejected, (state) => {
        state.token = "Unautorized!";
        localStorage.setItem("token", '')
      });
  }
});

export const { logout, setEmail } = authSlice.actions;
export default authSlice.reducer;