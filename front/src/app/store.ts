import { configureStore } from '@reduxjs/toolkit';
import authReducer from './customslice/authSlice';
import localizationReducer from './customslice/localizationslice';

export const store = configureStore({
    reducer: {
        auth: authReducer,
        localization: localizationReducer,
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware({
            serializableCheck: false,
        })
    },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;