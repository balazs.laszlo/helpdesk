import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { RootState, AppDispatch } from './store';

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export const useLocalizations = () => (useSelector as TypedUseSelectorHook<RootState>)((state) => state.localization.allStrings);
export const useIsLoggedIn = () => (useSelector as TypedUseSelectorHook<RootState>)(s => !!(s.auth.token));