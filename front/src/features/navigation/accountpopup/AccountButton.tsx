import { Button } from 'devextreme-react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector, useLocalizations } from '../../../app/hooks';
import { FontAwesome } from '../../../assets/FontAwesome';
import { logout } from '~/app/customslice/authSlice';

export type LoginButtonProps = {
  className?: string
}
export function LoginButton(props: LoginButtonProps) {
  const { className } = props;

  const isToken = useAppSelector(s => !!(s.auth.token));
  const dispatch = useAppDispatch();
  const strings = useLocalizations();
  const navigate = useNavigate();

  return (
    <Button
      className={className}
      text={
        isToken
          ? strings.getString('menu_logout')
          : strings.getString('menu_login')
      }
      icon={FontAwesome.user}
      id='navbar_account_btn'
      onClick={() => {
        if (isToken) {
          dispatch(logout())
        } else {
          navigate("login");
        }
      }} />
  );
}
