import { Button } from 'devextreme-react';
import { useAppDispatch, useIsLoggedIn, useLocalizations } from '../../../app/hooks';
import { logout } from '../../../app/customslice/authSlice';
import { FontAwesome } from '../../../assets/FontAwesome';
import { ButtonTooltip } from '../../../components/buttontooltips/ButtonTooltip';

export function AccountPopup() {
  const strings = useLocalizations();
  const dispatch = useAppDispatch();

  const isToken = useIsLoggedIn();

  return <ButtonTooltip
    show={isToken}
    target={"#navbar_account_btn"}
    renderChildren={(hide) => {
      return <>
        <Button
          text={strings.getString('menu_logout')}
          icon={FontAwesome.logout}
          onClick={() => {
            dispatch(logout());
            hide();
            window.location.reload();
          }} />
      </>
    }}
  />
}