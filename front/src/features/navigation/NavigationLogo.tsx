import Favicon from 'react-favicon';
import { useNavigate } from 'react-router-dom';
import { PartnerImage } from '../../components/partnerimage/PartnerImage';
import css from './NavigationLogo.module.scss'
import defaultLogo from '../../assets/logo.svg';

export function NavigationLogo() {
  const navigate = useNavigate();

  return (
    <div 
      onClick={() => navigate(`/`)}
      className={css.nav_logo}
    >
      <Favicon url={defaultLogo} />
      <PartnerImage
        name='logo'
        datauri={defaultLogo}
        height="35px" />
    </div>
  );
}
