import { TextBox } from 'devextreme-react';
import { useNavigate } from 'react-router-dom';
import { AccountPopup } from './accountpopup/AccountPopup';
import { LoginButton } from './accountpopup/AccountButton';
import { NavigationLogo } from './NavigationLogo';
import css from './Navigation.module.scss';

export default function Navigation() {
  const navigate = useNavigate();

  return (
    <div className={css.nav_header}>
      <div className={css.navigationholderouter}>
        <div className={css.navigationholder}>
          <NavigationLogo />

          <nav>
            <div>
            </div>
          </nav>

          <TextBox onEnterKey={(e) => {
            let val = e.component.option('value');
            if (val != null) {
              e.component.option('value', '')
              navigate("/search/" + encodeURIComponent(val));
            }
          }} />

          <div className={css.action_buttons}>
            <LoginButton
              className={css.row_padding}
            />
          </div>
        </div>
      </div >
    </div>
  );
}
