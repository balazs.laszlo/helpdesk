import axios from 'axios';
import { useAppSelector, useLocalizations } from '~/app/hooks';
import { Form, Popup } from 'devextreme-react';
import { GroupItem, SimpleItem } from 'devextreme-react/form';
import { ButtonItem } from 'devextreme-react/form';
import notify from 'devextreme/ui/notify';

type InnerPopupProps = {
  setAdd: React.Dispatch<React.SetStateAction<number>>;
  ticketPoupupRef: React.RefObject<Popup>;
};
export function InnerPopup(props: InnerPopupProps) {
  const { setAdd, ticketPoupupRef } = props;
  const token = useAppSelector(s => s.auth.token);
  const strings = useLocalizations();
  return <form action="your-action" onSubmit={(e) => {
    let form = e.target as HTMLFormElement;
    let data = new FormData(form);
    e.preventDefault();

    let title = data.get('title');
    let description = data.get('description');

    if (!title) {
      notify({
        message: strings.getString('missing_ticket_title'),
        position: {
          my: 'center top',
          at: 'center top',
        },
      }, 'error', 3000);
      return;
    }

    if (!description) {
      notify({
        message: strings.getString('missing_ticket_description'),
        position: {
          my: 'center top',
          at: 'center top',
        },
      }, 'error', 3000);
      return;
    }

    axios.post('/api/v1/tickets', {
      title,
      description,
    }, {
      headers: {
        authorization: 'Bearer ' + token,
      }
    }).then(resp => {
      form.reset();
      notify({
        message: strings.getString('ticket_saved'),
        position: {
          my: 'center top',
          at: 'center top',
        },
      }, 'success', 3000);
      setAdd(s => s + 1);
    });

    ticketPoupupRef.current?.instance.hide();
  }}>
    <Form>
      <GroupItem>
        <SimpleItem
          dataField="title"
          label={{ text: strings.getString('ticket_obj_title') }} />
        <SimpleItem
          dataField="description"
          editorType="dxTextArea"
          label={{ text: strings.getString('ticket_obj_description') }}
          editorOptions={{
            height: "200px"
          }} />
        <ButtonItem
          horizontalAlignment="right"
          buttonOptions={{
            text: strings.getString('save'),
            type: 'success',
            useSubmitBehavior: true,
          }} />
      </GroupItem>
    </Form>
  </form>;
}
