import css from './Home.module.scss'
import { useMemo, useState } from 'react';
import axios from 'axios';
import { Ticket } from '../../types/Ticket';
import { useHandleAxiosExcepons } from '../../hooks/useHandleAxiosExcepons';
import { useAppSelector, useLocalizations } from '~/app/hooks';
import { Popup, SpeedDialAction } from 'devextreme-react';
import { useRef } from 'react';
import { InnerPopup } from './InnerPopup';
import { TicketLine } from './TicketLine';
import Pagination from '~/components/pagination/Pagination';

export default function Home() {
  const handleExcepons = useHandleAxiosExcepons();
  const token = useAppSelector(s => s.auth.token)
  const strings = useLocalizations();
  const isSupport = useAppSelector(s => s.auth.isSupport)

  const [stockGroups, setStockGroups] = useState<any[] | null>(null);
  const [add, setAdd] = useState(0);
  const [pagecount, setPagecount] = useState(0);
  const [page, setPage] = useState(1);

  useMemo(() => {
    axios.get(`/api/v1/tickets?perPage=10&page=${page}`,
      {
        headers: {
          authorization: 'Bearer ' + token,
        }
      }).then(r => {
        if (Array.isArray(r.data)) {
          setStockGroups(r.data);
        } else if (r.data.tickets) {
          setStockGroups(r.data.tickets);
          setPagecount(r.data.totalPages)
        } else {
          setStockGroups((s) => {
            let arr = structuredClone(s ?? []);
            arr.push(r.data);
            return arr;
          });
        }
      }, (err) => {
        handleExcepons(err.response)
      })
  }, [add, page]);

  const ticketPoupupRef = useRef<Popup>(null)

  return (
    <div style={{
      width: '100%',
    }}>
      <div>
        {
          !isSupport &&
          <SpeedDialAction
            icon='add'
            label={strings.getString('new_ticket')}
            index={1}
            onClick={() => {
              ticketPoupupRef.current?.instance.show();
            }}
          />
        }
      </div>
      <div
        className={css.stock_group_grid}
      >
        {
          stockGroups?.map((stockGroup: Ticket) => {
            return <TicketLine
              key={stockGroup._id}
              ticket={stockGroup}
            />
          })
        }
      </div>
      {
        pagecount > 1 &&
        <Pagination
          handlePageClick={(e) => {
            let v = e.selected + 1;
            if (e.selected + 1 != page) {
              setStockGroups([])
              setPage(v)
            }
          }}
          displayedPageRange={3}
          pageCount={pagecount}
        />
      }
      <Popup
        width={"600px"}
        height={"auto"}
        ref={ticketPoupupRef}
        showCloseButton={true}
        dragEnabled={false}
        title={strings.getString('new_ticket')}
      >
        <InnerPopup setAdd={setAdd} ticketPoupupRef={ticketPoupupRef} />
      </Popup>
    </div>
  )
}
