import moment from 'moment';
import { Ticket } from '../../types/Ticket';
import css from './StockHolderCard.module.scss';
import { useNavigate } from 'react-router';
import { useAppSelector, useLocalizations } from '~/app/hooks';
import { useMemo, useState } from 'react';
import axios from 'axios';

type TicketLineProps = {
  ticket: Ticket;
};
export function TicketLine(props: TicketLineProps): JSX.Element {
  const { ticket: stockGroup } = props;
  const strings = useLocalizations();
  const token = useAppSelector(s => s.auth.token)
  const isSupport = useAppSelector(s => s.auth.isSupport)
  const navigate = useNavigate();
  
  const [username, setUsername] = useState(null);
  useMemo(() => {
    if (isSupport) {
      axios.get(`/api/v1/users/${stockGroup.userId}`,
        {
          headers: {
            authorization: 'Bearer ' + token,
          }
        }).then(r => {
          console.log(r);
          setUsername(r.data?.username)
        });
    }
  }, [stockGroup._id])
  
  return (<div className={css.stock_holder_card} onClick={onClick}>
    <strong>
      <p className={css.stock_name}>{stockGroup.title}</p>
    </strong>
    <div>
      <strong>{strings.getString('status')}: </strong>
      <span className={css.stock_name}>{stockGroup.state}</span>
    </div>
    {username &&
      <div>
        <strong>{strings.getString('user')}: </strong>
        <span className={css.stock_name}>{username}</span>
      </div>
    }
    <p className={css.stock_name}>{moment(stockGroup.createdAt).format('LLLL')}</p>
  </div>)

  function onClick () {
    navigate(`/ticket/${stockGroup._id}`)
  }
}
