import { useMemo, useState } from 'react';
import axios from 'axios';
import { Ticket } from '../../types/Ticket';
import { useHandleAxiosExcepons } from '../../hooks/useHandleAxiosExcepons';
import { useAppSelector, useLocalizations } from '~/app/hooks';
import { Lookup, TextArea } from 'devextreme-react';
import { useParams } from 'react-router';
import { DropDownOptions } from 'devextreme-react/autocomplete';
import { NativeEventInfo } from 'devextreme/events';
import dxLookup from 'devextreme/ui/lookup';
import qs from 'qs';

export default function TicketDetails() {
  const { identifier } = useParams();

  const handleExcepons = useHandleAxiosExcepons();
  const token = useAppSelector(s => s.auth.token)
  const isSupport = useAppSelector(s => s.auth.isSupport)
  const strings = useLocalizations();

  const [ticket, setTicket] = useState<Ticket | null>(null)
  const [username, setUsername] = useState(null);
  const [newComment, setNewComment] = useState('')
  const [update, forceUpdate] = useState(true)

  const statuses = [
    "sent",
    "processing",
    "solved",
  ]
  useMemo(() => {
    if (isSupport && ticket?.userId) {
      axios.get(`/api/v1/users/${ticket?.userId}`,
        {
          headers: {
            authorization: 'Bearer ' + token,
          }
        }).then(r => {
          setUsername(r.data?.username)
        });
    }
  }, [ticket?._id])

  useMemo(() => {
    axios.get(`/api/v1/tickets/${identifier}`,
      {
        headers: {
          authorization: 'Bearer ' + token,
        }
      }).then(r => {
        setTicket(r.data)
      }, (err) => {
        handleExcepons(err.response)
      });
  }, [update]);

  return (
    <div style={{
      width: '100%',
      padding: '3%'
    }}>
      <h3>{ticket?.title}</h3>
      <div><span style={{ fontWeight: 'bold' }}>{strings.getString('status')}:</span>
        {
          isSupport && ticket?
            <div style={{
              display: 'inline-block'
            }}>
              <Lookup
                items={statuses}
                defaultValue={ticket.state}
                onValueChange={(e) => {
                  if (ticket) {
                    ticket.state = e;
                    saveChange(ticket)
                  }
                }}
              >
                <DropDownOptions
                  showTitle={false}
                  width={200}
                />
              </Lookup>
            </div>
            : ticket?.state
        }</div>
      {
        username &&
        <p><span style={{ fontWeight: 'bold' }}>{strings.getString('user')}:</span> {username}</p>
      }
      <div><span style={{ fontWeight: 'bold' }}>{strings.getString('description')}:</span> {ticket?.description}</div>
      {
        ticket && ticket.comments?.length > 0 &&
        ticket.comments.map(e => {
          return <div key={e}>{e}</div>
        })
      }
      <p style={{ fontWeight: 'bold' }}>{strings.getString('cart_comment')}:</p>
      <TextArea
        height={90}
        value={newComment}
        onValueChanged={(e) => { console.log("alma"); setNewComment(e.value) }}
      />

    </div>
  )

  function saveChange(changeObj: any) {
    console.log(changeObj);

    let data = qs.stringify(changeObj);

    axios.put(`http://localhost:1234/api/v1/tickets/${identifier}`, data, {
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    })
      .then((response) => {
        console.log(JSON.stringify(response.data));
        forceUpdate(s => !s);
      })
      .catch((error) => {
        console.log(error);
      });
  }
}
