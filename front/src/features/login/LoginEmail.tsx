import css from './Login.module.scss';
import { useAppDispatch, useLocalizations } from '../../app/hooks';
import React, { useState } from 'react';
import { isLogin, setEmail, setUsername } from '../../app/customslice/authSlice';
import { useNavigate } from 'react-router';
import { othererror } from './Login';
import notify from 'devextreme/ui/notify';
import { TextBox } from 'devextreme-react';
import { ValidationStatus } from 'devextreme/ui/validator';


export function LoginEmail() {
  const strings = useLocalizations();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [emailValidationStatus, setEmailValidationStatus] = useState<ValidationStatus>("valid");
  const [emailValidationErrors, setEmailValidationErrors] = useState([{ message: "" }]);

  const [email, _setEmail] = useState("");

  const loginWithEmail = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (email.length == 0) {
      notify(strings.getString('login_error_missing_username_or_email'), "error", 3000)
      return;
    }

    dispatch(isLogin({
      email,
    })).then((e) => {
      console.log(e);

      if (e.payload.message?.error?.startsWith("29")) {
        dispatch(setEmail(email));
        if (email == "") {
          setEmailValidationErrors([{ message: strings.getString("login_error_email") }])
          setEmailValidationStatus("invalid");
        } else {
          setEmailValidationStatus("valid");
          navigate('/login/register');
        }
      } else if (e.payload.token) {
        dispatch(setEmail(email));
        navigate('/login/pw');
      } else {
        othererror();
      }
    }).catch(e => {
      othererror();
    });
  };
  return (
    <>
      <div className={css.auth_panel_header}>
        <h1>{strings.getString('login_title')}</h1>
      </div>
      <div className={css.auth_panel_body}>
        <form name="user_login" onSubmit={loginWithEmail} noValidate={false}>
          <div className={css.form_group}>
            <label
              htmlFor="user_login_email"
            >
              {strings.getString('login_email')}
            </label>
            <TextBox
              mode="email"
              id="user_login_email"
              showClearButton={true}
              className={css.form_control}
              onValueChange={(value) => { _setEmail(value) }}
              /* @ts-ignore */
              validationMessagePosition="bottom"
              validationStatus={emailValidationStatus}
              validationErrors={emailValidationErrors}
            />
          </div>
          <div className={css.form_group}>
            <button type="submit">{strings.getString('login_continue')}</button>
          </div>
        </form>
      </div>
    </>
  );
}
