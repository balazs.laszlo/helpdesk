import css from './Login.module.scss';
import { useAppDispatch, useAppSelector, useLocalizations } from '../../app/hooks';
import React from 'react';
import { register as registerBackend, setEmail } from '../../app/customslice/authSlice';
import { useNavigate } from 'react-router';
import { alert } from 'devextreme/ui/dialog';
import { othererror } from './Login';
import { goBack } from "./goBack";
import { useIsLogin } from "./useIsLogin";


export function LoginRegister() {
  const strings = useLocalizations();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const email = useAppSelector(s => s.auth.email) ?? "";

  useIsLogin();

  const registerMe = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    let form = new FormData(e.currentTarget);

    let password = form.get('password') as string;
    let password2 = form.get('password2') as string;

    if (password != password2) {
      alert(strings.getString('register_pasword_missmatch'), "");
      return;
    }

    let agreeTerms = form.get('agree_terms') as string;
    let canSendNotifications = form.get('can_send_notifications') as string;

    let name = form.get('username') as string;
    console.log({ email, password });
    dispatch(registerBackend({
      email,
      password,
      name,
      agreeTerms,
      canSendNotifications,
    })).then(e => {
      if (e.payload?.error) {
        othererror();
      } if (e.payload?.registered) {
        dispatch(setEmail(email));
        navigate('/login/pw');
      } else {
        othererror();
      }
    }).catch(e => {
      othererror();
    });
  };
  return (
    <>
      <div className={css.auth_panel_header}>
        <h1>{strings.getString('register_title')}</h1>
        <div className={css.slot_back}>
          <a onClick={goBack.bind(null, navigate)} className="btn-back">{strings.getString('login_back')}</a>
        </div>
      </div>
      <div className={css.auth_panel_body}>
        <div className={css.small_title}>{strings.getString('register_small_title')}</div>
        <form name="user_login" onSubmit={registerMe} noValidate={false}>
          <div className={css.form_group}>
            <div className={css.email}>{email}</div>
          </div>
          <div className={css.form_group}>
            <label
              htmlFor="user_login_name"
            >
              {strings.getString('register_username')}
            </label>
            <input
              type="text"
              id="user_login_name"
              name="username"
              required
              autoFocus={false}
              className={css.form_control} />
          </div>
          <div className={css.form_group}>
            <label
              htmlFor="user_login_password"
            >
              {strings.getString('register_password')}
            </label>
            <input
              type="password"
              id="user_login_password"
              name="password"
              required
              data-constraint="password"
              autoFocus={false}
              className={css.form_control} />
          </div>
          <div className={css.form_group}>
            <label
              htmlFor="user_login_password2"
            >
              {strings.getString('register_password2')}
            </label>
            <input
              type="password"
              id="user_login_password2"
              name="password2"
              required
              data-constraint="password"
              autoFocus={false}
              className={css.form_control} />
          </div>
          <div className={css.form_group}>
            <div className={css.form_check}>
              <input
                type="checkbox"
                id="register_terms_conditions"
                name="agree_terms"
                required
                data-constraint="Required"
                value="1" />
              <label
                htmlFor="register_terms_conditions"
              >
                {strings.getString('register_terms_conditions')}
              </label>
            </div>
          </div>
          <div className={css.form_group}>
            <div className={css.form_check}>
              <input
                type="checkbox"
                id="register_can_send_notifications"
                name="can_send_notifications"
                value="1" />
              <label
                htmlFor="register_can_send_notifications"
              >
                {strings.getString('register_can_send_notifications')}
              </label>
            </div>
          </div>
          <div className={css.form_group}>
            <button type="submit">{strings.getString('login_continue')}</button>
          </div>
        </form>
      </div>
    </>
  );
}
