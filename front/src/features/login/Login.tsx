import css from './Login.module.scss'
import { Outlet } from 'react-router';
import { alert } from 'devextreme/ui/dialog';

export default function LoginMain() {
    return (
        <div>
            <div className={css.hero_grid}>
                <div></div>
                <div className={css.hero} >
                    <div className={css.auth_panel}>
                        <Outlet />
                    </div>
                </div>
                <div></div>
            </div>
            <div>
            </div>
        </div>
    )
}

export function othererror() {
    let errormsg = 'The server is currently canot handdle the request due to big number of requests please return later!';
    alert(errormsg, "Error");
}