import { useAppSelector } from '../../app/hooks';
import { useEffect } from 'react';
import { useNavigate } from 'react-router';


export function useIsLogin() {
  const navigate = useNavigate();
  const email = useAppSelector(s => s.auth.email);
  const username = useAppSelector(s => s.auth.username);
  const isLoggedin = useAppSelector(s => !!(s.auth.token));
  useEffect(() => {
    if (!email && !username && !isLoggedin) {
      navigate('/login');
    }
  });
}
