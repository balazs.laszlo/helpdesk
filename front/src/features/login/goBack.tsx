import { setEmail } from '../../app/customslice/authSlice';

export function goBack(navigate: Function) {
  setEmail(undefined);
  navigate('/login');
}
