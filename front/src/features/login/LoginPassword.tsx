import css from './Login.module.scss';
import { useAppDispatch, useAppSelector, useLocalizations } from '../../app/hooks';
import React, { useState } from 'react';
import { login, setEmail } from '../../app/customslice/authSlice';
import { useNavigate } from 'react-router';
import { othererror } from './Login';
import { goBack } from "./goBack";
import { useIsLogin } from "./useIsLogin";
import { TextBox } from 'devextreme-react';
import { ValidationStatus } from 'devextreme/common';

export function LoginPassword() {
  const strings = useLocalizations();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const email = useAppSelector(s => s.auth.email);
  const [pwd, setPwd] = useState("");

  const [validationStatus, setValidationStatus] = useState<ValidationStatus>("valid");
  const [validationErrors, setValidationErrors] = useState([{ message: "" }]);

  useIsLogin();
  if (!email) {
    return <></>;
  }

  const loginWithPass = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    let password = pwd;
    console.log({ email, password });
    dispatch(login({
      email,
      password
    })).then(r => {
      if (r.payload?.token) {
        navigate('/');
        dispatch(setEmail(undefined));
      } else {
        setValidationStatus("invalid");
        setValidationErrors([{ message: strings.getString("login_error_password")}])
      }
    }).catch(e => {
      othererror();
    });
  };
  return (
    <>
      <div className={css.auth_panel_header}>
        <h1>{strings.getString('login_title')}</h1>
        <div className={css.slot_back}>
          <a onClick={goBack.bind(null, navigate)} className="btn-back">înapoi</a>
        </div>
      </div>
      <div className={css.auth_panel_body}>
        <div className={css.auth_avatar}></div>
        <form name="user_login" onSubmit={loginWithPass} noValidate={false}>
          <div className={css.form_group}>
            <div className={css.email}>{email}</div>
          </div>
          <div className={css.form_group}>
            <label
              htmlFor="user_login_email"
            >
              {strings.getString('login_password')}
            </label>
              <TextBox 
                mode="password"
                id="user_login_password"
                showClearButton={true}
                className={css.form_control}
                onValueChange={(value)=> { setPwd(value) }}
                /* @ts-ignore */
                validationMessagePosition="bottom"
                validationStatus={validationStatus}
                validationErrors={validationErrors}
              />
          </div>
          <div className={css.form_group}>
            <button type="submit">{strings.getString('login_continue')}</button>
          </div>
        </form>
        <div className={css.forgotten}>
          <a href={"/forgot-password?email=" + email}>Ai uitat parola?</a>
        </div>
      </div>
    </>
  );
}
