import axios from 'axios';
import { useMemo, useState } from 'react';
import css from './Search.module.scss'
import { useParams } from 'react-router-dom';
import { useAppSelector, useLocalizations } from '~/app/hooks';
import { useHandleAxiosExcepons } from '~/hooks/useHandleAxiosExcepons';
import { Ticket } from '~/types/Ticket';
import { TicketLine } from '../home/TicketLine';
import Pagination from '~/components/pagination/Pagination';

function Search() {
  const handleExcepons = useHandleAxiosExcepons();
  let strings = useLocalizations();
  const token = useAppSelector(s => s.auth.token)

  let { keywords: keyword } = useParams();
  if (keyword == null) {
    keyword = '';
  }
  let keywords = keyword.split(" ")

  const [stockGroups, setStockGroups] = useState<any[] | null>(null);
  const [pagecount, setPagecount] = useState(0);
  const [page, setPage] = useState(1);

  useMemo(() => {
    let k = encodeURIComponent(JSON.stringify(keywords));
    axios.get(`/api/v1/tickets?perPage=10&page=${page}&keywords=${k}`,
      {
        headers: {
          authorization: 'Bearer ' + token,
        }
      }).then(r => {
        if (Array.isArray(r.data)) {
          setStockGroups(r.data);
        } else if (r.data.tickets) {
          setStockGroups(r.data.tickets);
          setPagecount(r.data.totalPages)
        } else {
          setStockGroups((s) => {
            let arr = structuredClone(s ?? []);
            arr.push(r.data);
            return arr;
          });
        }
      }, (err) => {
        handleExcepons(err.response)
      })
  }, [page,keyword]);

  return (
    <div style={{
      width: '100%'
    }}>
      <h1>{strings.getString("search_results")}</h1>
      <h3>{strings.getString("search_keywords")}: {keywords.join(" ")}</h3>

      <div
        className={css.stock_group_grid}
      >
        {
          stockGroups?.map((stockGroup: Ticket) => {
            return <TicketLine
              key={stockGroup._id}
              ticket={stockGroup}
            />
          })
        }
      </div>
      {
        pagecount > 1 &&
        <Pagination
          handlePageClick={(e) => {
            let v = e.selected + 1;
            if (e.selected + 1 != page) {
              setStockGroups([])
              setPage(v)
            }
          }}
          displayedPageRange={3}
          pageCount={pagecount}
        />
      }

    </div>
  );
}

export default Search