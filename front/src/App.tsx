import './App.scss'
import { BrowserRouter } from "react-router-dom";
import 'devextreme/dist/css/dx.light.css';
import { Routes, Route } from "react-router-dom";
import Home from './features/home/Home';
import Error404 from './features/error404/Error404';
import Search from './features/search/Search';
import LoginMain from './features/login/Login';
import { LoginEmail } from "./features/login/LoginEmail";
import { LoginPassword } from "./features/login/LoginPassword";
import { LoginRegister } from "./features/login/LoginRegister";
import moment from 'moment';
import { loadMessages, locale } from 'devextreme/localization';
import languages from './assets/dx.messages.ro.json';
import { Title } from './wrappers/Title';
import { NavigationWrapper } from './wrappers/NavigationWrapper';
import TicketDetails from './features/home/TicketDetails';

export default function App() {
  setLocale();

  return (
    <BrowserRouter>
      <Routes>

        <Route path="login" element={<Title titleKey='menu_login_title'><LoginMain /></Title>}>
          <Route index element={<LoginEmail />} />
          <Route path='pw' element={<LoginPassword />} />
          <Route path='register' element={<LoginRegister />} />
        </Route>

        <Route path='' element={<NavigationWrapper />}>
          <Route index element={<Title titleKey='none' title={location.hostname.substring(0, 1).toUpperCase() + location.hostname.substring(1)}><Home /></Title>} />
          <Route path="ticket/:identifier" element={<TicketDetails />} />
          <Route path="search" element={<Search />}>
            <Route path=":keywords" element={<Title titleKey='search_title'><Search /></Title>} />
          </Route>
        </Route>


        <Route path="*" element={<Title titleKey='error404'><Error404 /></Title>} />
      </Routes>
    </BrowserRouter>
  );
  function setLocale() {
    // Devextrame translatetion
    loadMessages(languages);
    locale("ro");

    // moment translatetion
    // TODO: finish translation from french
    moment.locale("ro", {
      months: ['Januarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie'],
      monthsShort: 'jan._feb._mar._apr._mai_iun._iul._aug._sept._oct._nov._dec.'.split('_'),

      monthsParseExact: true,
      weekdays: 'duminică_luni_marţi_miercuri_joi_vineri_sâmbătă'.split('_'),
      weekdaysShort: 'dum._lun._mar._mir._joi_vin._sâm.'.split('_'),
      weekdaysMin: 'Du_Lu_Ma_Mi_Jo_Vi_Sâ'.split('_'),
      weekdaysParseExact: true,
      longDateFormat: {
        LT: 'HH:mm',
        LTS: 'HH:mm:ss',
        L: 'DD/MM/YYYY',
        LL: 'D MMMM YYYY',
        LLL: 'D MMMM YYYY, HH:mm',
        LLLL: 'dddd D MMMM YYYY, HH:mm'
      },
      // calendar : {
      //     sameDay : '[Aujourd’hui à] LT',
      //     nextDay : '[Demain à] LT',
      //     nextWeek : 'dddd [à] LT',
      //     lastDay : '[Hier à] LT',
      //     lastWeek : 'dddd [dernier à] LT',
      //     sameElse : 'L'
      // },
      // relativeTime : {
      //     future : 'dans %s',
      //     past : 'il y a %s',
      //     s : 'quelques secondes',
      //     m : 'une minute',
      //     mm : '%d minutes',
      //     h : 'une heure',
      //     hh : '%d heures',
      //     d : 'un jour',
      //     dd : '%d jours',
      //     M : 'un mois',
      //     MM : '%d mois',
      //     y : 'un an',
      //     yy : '%d ans'
      // },
      dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
      // ordinal : function (number) {
      //     return number + (number === 1 ? 'er' : 'e');
      // },
      week: {
        dow: 1,
        doy: 4
      }
    });
  }
}