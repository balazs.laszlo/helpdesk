module.exports = {
    extends: 'standard-with-typescript',
    parser: '@typescript-eslint/parser',
    parserOptions: {
        tsconfigRootDir: __dirname,
        project: ['./tsconfig.json'],
    },
    plugins: [
        '@typescript-eslint'
    ],
    root: true,
    rules: {
        "function-paren-newline": ["warn", { "minItems": 3 }],
        "semi-style": ["warn", "last"],
        "semi": ["warn", "always"],
        "comma-dangle": ["warn", { "arrays": "always", }],
        "indent": ["warn", 2]
    },
}