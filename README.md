Start the project: docker compose up -d

- Create user:
  - Click: Autenticare
  - Add email + Enter
  - Complete register form
  - Check the first checkbox and if you want to be support user, check the second also
- with normal user you can add tickets and see your existing ones
- With support user you can see other users tickets and edit its status
